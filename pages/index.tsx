import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import useTranslation from 'next-translate/useTranslation';
import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import { Dropdown, DropdownButton, Nav } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Carousel from 'react-bootstrap/Carousel'
import dynamic from "next/dynamic";
import jQuery from "jquery";
import 'aos/dist/aos.css';
import { useEffect } from "react";
import Link from 'next/link';

const Home: NextPage = () => {
  const { t } = useTranslation();
  // {t('description')}

  return (
    <div className='antialiased'>
      <Navbar></Navbar>
      <div className="service-bg">
        <main>


          <Carousel fade>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="/slide/slide-01.webp?text=First slide&bg=373940"
                alt="First slide"
              />
              <Carousel.Caption>
                <div className="container mt-5 pt-5 bg-info rounded mb-5" >
                  <div className="section-title">
                    <h3>First slide label</h3>
                    <p>First slide labelFirst slide labelFirst slide labelFirst slide labelFirst slide labelFirst slide label</p>
                  </div>



                </div>

              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="/slide/slide-02.webp?text=Second slide&bg=282c34"
                alt="Second slide"
              />

              <Carousel.Caption>
                <div className="container mt-5 pt-5 bg-info rounded" >
                  <div className="section-title">
                    <h3>Second slide label</h3>
                    <p>Second slide Second slide Second slide Second slide Second slide label</p>
                  </div>



                </div>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>

          <div className='col-md-12'>
            <section id="services" className="team services section-bg">
              <div className="HomeService">
                <img src="/BC_ModernOffice.webp" className="d-block w-100" />
                <h3 className="sub-tab">We believe in collaboration.By working together, we can achieve so many things.</h3>
                <p className="paragraphfont">We offer a lot of services. We can make any kind of website for your needs. We can develop web applications to solve complex tasks and to achieve specific functionalities for your works. We can develop android applications if you need an android app for your business.</p>
                <div className="read-more">
                  <Link href="/service">Services</Link>
                </div>
              </div>
            </section>
          </div>
        </main>
      </div>

      <div className="row">
        <div className="col-md-12">
          <section id="clients" className="clients">
            <div className="container" data-aos="fade-up">
              <div className="section-title">
                <h2>Our Clients</h2>
              </div>
              <div className="row no-gutters clients-wrap clearfix" data-aos="fade-up">
                <div className="col-lg-4 col-md-4 col-4">
                  <div className="client-logo">
                    <img src="/clients/client-01.png" className="img-fluid "
                      alt="" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-4">
                  <div className="client-logo">
                    <img src="/clients/client-02.png" className="img-fluid "
                      alt="" />
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-4">
                  <div className="client-logo">
                    <img src="/clients/client-03.jpg" className="img-fluid"
                      alt="" />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
      <Footer></Footer>
    </div>

  );

};

export default Home
