import { NextPage } from 'next';
import Navbar from '../components/layout/navbar';
import { useRouter } from 'next/router'
import Link from 'next/link';

const Role: NextPage = () => {
    const router = useRouter()
  const { role } = router.query
    
    return (
        <div>
            <Navbar></Navbar>
            <div className="container mb-5 mt-3" id="seniorJobDetail" style={{ display: role === 'senior' ? 'block' : 'none' }}>
                <div className="row section-title justify-content-center">
                    <h3 className="text-info mt-5">SENIOR DEVELOPER</h3>
                </div>
                <div className="row mt-5">
                    <div className="col-md-4 text-center">
                        <img src="../career/graduate.png" />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src="../career/it.png" />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src="../career/japanese.png" />
                    </div>
                </div>
                <div className="card mt-5">
                    <h3 className="text-info">Job Descriptions</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Create web application by using JAVA, PHP, C#, NEXT JS, REACT, .NET, NODEJS,...</li>
                            <li>Specification, design, coding, testing, release as well as maintenance of software programs.</li>
                            <li>You may also be closely tasked with GM (PM) and team members.</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Requirements</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Any Graduated</li>
                            <li>More than 3 years in full stack or frontend/backend software development position</li>
                            <li>More than 2 years experience in Database</li>
                            <li>Ability to write good technical documentation.</li>
                            <li>Japanese language proficiency level above N4 or if not we provide training)</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Recruitment Process</h3>
                    <h3 className="text-info mt-5">Benefits</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Free for Japanese language class</li>
                            <li>Saturday, Sunday Holiday</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Career Opportunities</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Many chance to work in Japan</li>
                            <li>Can be leader in near future</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Message</h3>
                    <div className="row mt-3">
                        <div className="col-md-2">Employment Type</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Full Time</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Days</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monday~Friday</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Hours</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">8:30~17:30 (Including 1 hour lunch break)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Place</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Room.204, No.5, Grand Nawaday Condo, Nawaday Street, Dagon Township.Yangon, Myanmar.</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Salary</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monthly Salary System (Company provides preferential treatments by considering the experience and ability etc..)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Allowance</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Qualification Allowance / Overtime Allowance</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Wage Raise </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Twice times in a year (Jan, July)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Welfare Program  </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Marriage Allowance/ Funeral Service Allowance/ Medical Checkup </div>
                    </div>
                    <div className="text-right">
                        <Link href="/careers" className="btnColor mt-3" role="button">&#8678;&nbsp;Back</Link>
                    </div>
                </div>
            </div>

            <div className="container mb-5 mt-3" id="juniorJobDetail" style={{ display: role === 'junior' ? 'block' : 'none' }}>
                <div className="row section-title justify-content-center text-centers">
                    <h3 className="text-info mt-5">JUNIOR DEVELOPER</h3>
                </div>
                <div className="row mt-5">
                    <div className="col-md-6 text-right ">
                        <img src="../career/graduate.png" className="mr-5 pl-5 pr-5" />
                    </div>
                    <div className="col-md-6">
                        <img src="../career/it.png" className="ml-5 pl-5 pr-5" />
                    </div>
                </div>
                <div className="card mt-5">
                    <h3 className="text-info">Job Descriptions</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Assisting the web development team with all aspects of website and application design.</li>
                            <li>Maintenance of Backend and Frontend applications.</li>
                            <li>You may also be closely tasked with GM (PM) and team members.</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Requirements</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Any Graduated</li>
                            <li>Strong technical skills and knowledge with the latest web development  technology of Git</li>
                            <li>Experience using code versioning systems such as GIT or SVN</li>
                            <li>Ability to write good technical documentation</li>
                            <li>Japanese language proficiency level above N4 or if not we provide training)</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Recruitment Process</h3>
                    <h3 className="text-info mt-5">Benefits</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Free for Japanese language class</li>
                            <li>Saturday, Sunday Holiday</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Career Opportunities</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Many chance to work in Japan</li>
                            <li>Can be leader in near future</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Message</h3>
                    <div className="row mt-3">
                        <div className="col-md-2">Employment Type</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Full Time</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Days</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monday~Friday</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Hours</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">8:30~17:30 (Including 1 hour lunch break)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Place</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Room.204, No.5, Grand Nawaday Condo, Nawaday Street, Dagon Township.Yangon, Myanmar.</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Salary</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monthly Salary System (Company provides preferential treatments by considering the experience and ability etc..)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Allowance</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Qualification Allowance / Overtime Allowance</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Wage Raise </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Twice times in a year (Jan, July)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Welfare Program  </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Marriage Allowance/ Funeral Service Allowance/ Medical Checkup </div>
                    </div>
                    <div className="text-right">
                        <Link href="/careers" className="btnColor mt-3" role="button">&#8678;&nbsp;Back</Link>
                    </div>
                </div>
            </div>

            <div className="container mb-5 mt-3" id="internJobDetail" style={{ display: role === 'intern' ? 'block' : 'none' }}>
                <div className="row section-title justify-content-center">
                    <h3 className="text-info mt-5">INTERNSHIP</h3>
                </div>
                <div className="row mt-5">
                    <div className="col-md-12 text-center">
                        <img src="../career/it.png" />
                    </div>
                </div>
                <div className="card mt-5">
                    <h3 className="text-info">Responsibilities</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Responsible for contributing to software design and software development</li>
                            <li>Debugging and testing code</li>
                            <li>Collaborates with other team members in creating secure and reliable software solution</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Requirements</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Any Graduated</li>
                            <li>Proactively learning about new technologies</li>
                            <li>Collaborate with our seniors on assignments</li>
                            <li>Strong Presentation Skills</li>
                            <li>Japanese language proficiency level above N4 or if not we provide training)</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info">Recruitment Process</h3>
                    <h3 className="text-info mt-5">Benefits</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Free for Japanese language class</li>
                            <li>Saturday, Sunday Holiday</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Career Opportunities</h3>
                    <div className="mt-3">
                        <ul>
                            <li>Many chance to work in Japan</li>
                            <li>Can be leader in near future</li>
                        </ul>
                    </div>
                    <hr />
                    <h3 className="text-info mt-5">Message</h3>
                    <div className="row mt-3">
                        <div className="col-md-2">Employment Type</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Full Time</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Days</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monday~Friday</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Hours</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">8:30~17:30 (Including 1 hour lunch break)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Working Place</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Room.204, No.5, Grand Nawaday Condo, Nawaday Street, Dagon Township.Yangon, Myanmar.</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Salary</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Monthly Salary System (Company provides preferential treatments by considering the experience and ability etc..)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Allowance</div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Qualification Allowance / Overtime Allowance</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Wage Raise </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Twice times in a year (Jan, July)</div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">Welfare Program  </div>
                        <div className="col-md-1">:</div>
                        <div className="col-md-9 pl-0">Marriage Allowance/ Funeral Service Allowance/ Medical Checkup </div>
                    </div>
                    <div className="text-right">
                        <Link href="/careers" className="btnColor mt-3" role="button">&#8678;&nbsp;Back</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Role;