import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import useTranslation from 'next-translate/useTranslation';
import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import { Dropdown, DropdownButton, Nav } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Carousel from 'react-bootstrap/Carousel'
import dynamic from "next/dynamic";
import jQuery from  "jquery";
import 'aos/dist/aos.css';
import { useEffect } from "react";
const Service: NextPage = () => {
  const { t } = useTranslation();

 
  return (
    <div className='antialiased'>
    <Navbar></Navbar>
    <div className="service-bg">
        <main>
                <div className="container mt-5 pt-5" >
                    <div className="section-title">
                        <h2>Services</h2>
                        <p>Innovative Digital Transformation</p>
                    </div>

                   
                        
                </div>
                <div  className="servicePanel slide " data-aos="zoom-in">
                    <div className="servicePanel-inner p-1"  role="listbox">
                        <div className="servicePanel-item " data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        <img src="/services/service-01.jpg " className="d-block w-100" alt="..." />
                        </div>
                        <div className="servicePanel-container-right col-md-6"   data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                            <div className=" servicePanel-content">
                                <h2>Services
                                </h2>

                                <span>Ser to the vic with S Ser to the vic with S Ser to the vic with S Ser to the vic with S</span>
                            </div>

                                 
 
                        </div>
                   
                    </div>
                </div>
                <div  className="servicePanel slide " data-aos="zoom-in">
                    <div className="servicePanel-inner p-2"  role="listbox">
                        <div className="servicePanel-item "  data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        <img src="/services/service-04.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className=" servicePanel-container-left col-md-6" data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                            <div className=" servicePanel-content">
                                <h2>Services
                                </h2>

                                <span>Ser to the vic with S Ser to the vic with S Ser to the vic with S Ser to the vic with S</span>
                            </div>  
                        </div>
                   
                    </div>
                </div>
                <div  className="servicePanel slide " data-aos="zoom-in">
                    <div className="servicePanel-inner p-2"  role="listbox">
                        <div className="servicePanel-item" data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        <img src="/services/service-02.jpg " className="d-block w-100" alt="..." />
                        </div>
                        <div className="servicePanel-container-right col-md-6"   data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                            <div className=" servicePanel-content">
                                <h2>Services
                                </h2>

                                <span>Ser to the vic with S Ser to the vic with S Ser to the vic with S Ser to the vic with S</span>
                            </div>  
                        </div>
                   
                    </div>
                </div>
                <div  className="servicePanel slide " data-aos="zoom-in">
                    <div className="servicePanel-inner p-2"  role="listbox">
                        <div className="servicePanel-item "  data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        <img src="/services/service-03.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className=" servicePanel-container-left col-md-6" data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                            <div className=" servicePanel-content">
                                <h2>Services
                                </h2>

                                <span>Ser to the vic with S Ser to the vic with S Ser to the vic with S Ser to the vic with S</span>
                            </div>  
                        </div>
                   
                    </div>
                </div>
                <div  className="servicePanel slide " data-aos="zoom-in">
                    <div className="servicePanel-inner p-2"  role="listbox">
                        <div className="servicePanel-item" data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        <img src="/services/service-05.jpg " className="d-block w-100" alt="..." />
                        </div>
                        <div className="servicePanel-container-right col-md-6"   data-aos="fade-left"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                            <div className=" servicePanel-content">
                                <h2>Services
                                </h2>

                                <span>Ser to the vic with S Ser to the vic with S Ser to the vic with S Ser to the vic with S</span>
                            </div>  
                        </div>
                   
                    </div>
                </div>
        </main>
        </div>
        <div className="technology-title">
        <h2>Technologies</h2>
        <div className="technology-icons">
            <div className="technology-icon">
                <img src="/technology/html.svg" alt="HTML" width="60" height="61" />
            </div>
            <div className="technology-icon">
                <img src="/technology/css.svg" alt="CSS" width="60" height="61" /> 
            </div>
            <div className="technology-icon">
                <img src="/technology/bootstrap.svg" alt="Bootstrap" width="50" height="56" />
            </div>
            <div className="technology-icon">
                <img src="/technology/js.svg" alt="JS" width="60" height="61" />
            </div>
            <div className="technology-icon">
                <img src="/technology/php.svg" alt="PHP" width="86" height="100" />
            </div>
            <div className="technology-icon">
                <img src="/technology/reactjs.svg" alt="ReactJs" width="80" height="100" />
            </div>
            <div className="technology-icon">
                <img src="/technology/nextjs.svg" alt="NextJs" width="80" height="100" />
            </div>
            <div className="technology-icon">
                <img src="/technology/nodejs.svg" alt="NodeJs" width="100" height="80" />
            </div>
            <div className="technology-icon">
                <img src="/technology/laravel.svg" alt="Laravel" width="80" height="100" />
            </div>
            <div className="technology-icon">
                <img src="/technology/go-programming-language.svg" alt="Go" width="100" height="100" />
            </div>
            <div className="technology-icon">
                <img src="/technology/c-sharp.svg" alt="C#" width="100" height="80" />
            </div>
            <div className="technology-icon">
                <img src="/technology/mysql.svg" alt="MySql" width="100" height="70" />
            </div>
            <div className="technology-icon">
                <img src="/technology/sqlite.svg" alt="Sqlite" width="100" height="70" />
            </div>
            <div className="technology-icon">
                <img src="/technology/postgresql.svg" alt="Postgresql" width="100" height="70" />
            </div>
            <div className="technology-icon">
                <img src="/technology/ubuntu.svg" alt="Ubuntu" width="100" height="80" />
            </div>
        </div>
    </div>
    <Footer></Footer>
    </div>
  );

};

export default Service
