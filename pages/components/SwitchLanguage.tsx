import React from 'react';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import i18nConfig from '../../i18n.json';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';

const SwitchLanguage = () => {
  const { locales, defaultLocale } = i18nConfig;
  const { t, lang } = useTranslation('common');

  return (
    <div>
      <DropdownButton id="dropdown-basic-button" title={lang}>
        {locales.map(lng => {
          if (lng === lang) return null;
          return (
            <Dropdown.Item key={lng}>
              <Link href="/" locale={lng} key={lng}>
                {t(`${lng}`)}
              </Link>
            </Dropdown.Item>
          );
        })}
      </DropdownButton>
    </div>
  );
};

export default SwitchLanguage;