import React from 'react';
import { Col, Container, Dropdown, DropdownButton, Row } from 'react-bootstrap';
import i18nConfig from '../../../i18n.json';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import Image from 'next/image'

const SwitchLanguage = () => {
    const { locales, defaultLocale } = i18nConfig;
    const { t, lang } = useTranslation('common');

    return (
        <header id="header" className='fixed-top'>
        <div className='container d-flex align-items-center'>
            <h1 className='logo mr-auto'>
                <Link href="" className='logo mr-auto'><img src="/logo.webp" alt=""
                    width="60" height="80" /></Link>
                <span>my</span>Sol
            </h1>
            <nav className='nav-menu d-none d-lg-block'>
                <ul>
                    <li><Link href="/">Home</Link></li>
                    <li><Link href="/service">Services</Link>
                    </li>
                    <li><Link href="/contact">Contact</Link>
                    </li>
                    <li><Link
                            href="/careers">Careers</Link>
                    </li>
                    <li><Link href="about">About</Link></li>
                    <DropdownButton id="dropdown-basic-button" title={lang} className='nav-item dropdown ms-5'>
                        {locales.map(lng => {
                            if (lng === lang) return null;
                            return (

                                <Dropdown.Item key={lng}>

                                    <Link href="/" locale={lng} key={lng} className='nav-link dropdown-toggle'>
                                        {t(`${lng}`)}
                                    </Link>
                                </Dropdown.Item>

                            );
                        })}
                    </DropdownButton>
                </ul>
            </nav>
        </div>
    </header>
    );
};

export default SwitchLanguage;