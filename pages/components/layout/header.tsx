import React, { ReactNode } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Navbar } from 'react-bootstrap'; "./navbar";
type Props = {
  children: ReactNode;
  title?: string;
};

const Header = ({
  children,
  title = 'TypeScript Next.js Stripe Example',
}: Props) => (
  <>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@thorwebdev" />
      <meta name="twitter:title" content="MySol" />
      <meta
        name="twitter:description"
        content="Myanmar Solution Col.,Ltd."
      />
      <meta
        name="twitter:image"
        content="https://nextjs-typescript-react-stripe-js.now.sh/social_card.png"
      />
      
    </Head>
    <Navbar></Navbar>
  </>
);

export default Header;