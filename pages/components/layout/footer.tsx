import React, { useEffect } from 'react';
import { Col, Container, Dropdown, DropdownButton, Row } from 'react-bootstrap';
import i18nConfig from '../../../i18n.json';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import Image from 'next/image';
import 'react-icofont/src/icofont/icofont.min.css';

const SwitchLanguage = () => {
  // const { locales, defaultLocale } = i18nConfig;
  const { t, lang } = useTranslation('common');
  // useEffect(() => {
  //   window.navigator.geolocation.getCurrentPosition(
  //     (newPos) => defaultLocale(newPos),
  //     console.error
  //   );
  // }, [locales]);
  return (
    <div className="footer-top">
      <div className="contact">
        <div className="row justify-content-center" data-aos="">

          <div className="col-lg-12">

            <div className="info-wrap">
              <div className="row">
                <div className="col-lg-4 info">
                  <i className="icofont-google-map"></i>
                  <h4>Location:</h4>
                  <p>Room.204, No.5, Grand Nawaday Condo, Nawaday Street, Dagon Township.<br />Yangon, Myanmar.
                  </p>
                </div>

                <div className="col-lg-4 info mt-4 mt-lg-0">
                  <i className="icofont-envelope" />
                  <h4>Email:</h4>
                  <p className="ml-12">
                    futurecareerdream@gmail.com</p>
                </div>

                <div className="col-lg-4 info mt-4 mt-lg-0">
                  <i className="icofont-phone"></i>
                  <h4>Phone:</h4>
                  <p className="ml-12">
                    +95 9773792671, +81 08044600534</p>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
      <div className="container-copyright d-md-flex">
        <div className="mr-md-auto text-center">
          <div className="copyright">
            &copy; Copyright <strong><span>MySol Co., Ltd</span></strong>. All Rights Reserved.
            <a href="{{ URL::to('/termandcomdition') }}" target="_blank"> <b>Terms
              and
              Conditions</b></a> |
            <a href="{{ URL::to('/policy') }}" target="_blank"><b>Privacy
              Policy</b></a>
          </div>
          {/* <Icofont icon="bell"/> */}
        </div>
      </div>
    </div>
  );
};
export default SwitchLanguage;