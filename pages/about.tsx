import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import useTranslation from 'next-translate/useTranslation';
import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import { Dropdown, DropdownButton, Nav } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Carousel from 'react-bootstrap/Carousel'
import dynamic from "next/dynamic";
import jQuery from  "jquery";
import 'aos/dist/aos.css';import AOS from "aos";

import { useEffect } from "react";
const About: NextPage = () => {
  const { t } = useTranslation();

 
  return (
    <div className='antialiased'>
    <Navbar></Navbar>
    <main id="main">

    <section id="about-us" className="about-us">
        <div className="container">

            <div className="row content">
            </div>
            
        </div>
                    <div className="col-lg-12" data-aos="fade-left">
                        <div data-aos="zoom-in">
                        <h2>Myanmar Solution</h2>
                    </div>
                
                <h3 className="background-info">Background</h3>
                <p>Myanmar has a large number of talented technical engineers, but the economic downturn has led to job shortages, while in Japan there has been a shortage of qualified and young technical engineers and technological advances. Considering these situations, not only for those who want to gain work experience in Myanmar who are interested in technology, but also for those who have the potential to develop skills and competencies. It was established with the aim of creating a strong and strong Japan-Myanmar cooperation technology field that can produce qualified technicians.
                
                </p>
            </div>
                <div className="col-lg-12" data-aos="fade-right">
                    <h3 className="about-info">About</h3>
                </div>
            <div className=" row col-lg-12">
                <div className="col-lg-6" data-aos="fade-left">
                    <p>A software development company founded by a visionary IT professionals who are providing extending innovative software services on our customer’s journey to ‘Digital Transformation’. Our Head Office, MySol, is in Japan. We set up effective with new Myanmar engineers who are well trained and well educated and on the job training (OJT) programs at work and give good experiences to all.We care not only our high quality members and products but also clients&#039; satisfaction.We maintain our workplace to be excellent and productive.We provide expert technology advisory services to individuals and small businesses.
                    </p>
                </div>
                <div className="col-lg-6 aboutus-img" data-aos="fade-left">
                    <img src="/about/aboutus.jpg" alt=""
                    className="aboutus-image" />
                </div>
        </div>
    </section>

    <div className="py-5 team4">
        <div className="container">
          <div className="row justify-content-center mb-4">
            <div className="col-md-7 text-center business-title">
              <h2 className="mb-3">Business Organization</h2>
              <h5 className="subtitle">Manage the Lcoal and Oversea Team Members</h5>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 mb-4">
            
            </div>
           
        
            <div className="col-lg-3 mb-4">
              <div className="row">
                <div className="col-md-12">
                  <img src="/team/team-7.jpg" alt="上垣将人" className="img-fluid rounded-circle" />
                </div>
                <div className="col-md-12 text-center">
                  <div className="pt-2">
                    <h5 className="mt-4 font-weight-medium mb-0">Co-Founder & Consultant</h5>
                    <h6 className="subtitle mb-3">Mr.上垣将人</h6>
                    <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 mb-4">
                <div className="row">
                  <div className="col-md-12">
                    <img src="/team/team-1.jpg" alt="Kyaw Phyo Naing" className="img-fluid rounded-circle" />
                  </div>
                  <div className="col-md-12 text-center">
                    <div className="pt-2">
                      <h5 className="mt-4 font-weight-medium mb-0">General Manager</h5>
                      <h6 className="subtitle mb-3">Kyaw Phyo Naing</h6>
                      <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 mb-4">
            
            </div>
          </div>
        </div>

        <div className="container">
            <div className="row justify-content-center mb-4">
              <div className="col-md-7 text-center local-title">
                <h3 className="mb-3">Myanmar Team Members</h3>
                <h5 className="subtitle">Develop and Implement the incredible Application with up-to-date Technologies</h5>
              </div>
            </div>
            <div className="row">
                <div className="col-lg-3 mb-4">
                    <div className="row">
                      <div className="col-md-12">
                        <img src="/team/team-6.jpg" alt="Shoon Lai Moe Oo" className="img-fluid rounded-circle" />
                      </div>
                      <div className="col-md-12 text-center">
                        <div className="pt-2">
                          <h5 className="mt-4 font-weight-medium mb-0">HR Manager</h5>
                          <h6 className="subtitle mb-3">Shoon Lai Moe Oo</h6>
                          <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                      </div>
                    </div>
                  </div>
             
          
              <div className="col-lg-3 mb-4">
                <div className="row">
                  <div className="col-md-12">
                    <img src="/team/team-2.jpg" alt="Su Mon Kyaw" className="img-fluid rounded-circle" />
                  </div>
                  <div className="col-md-12 text-center">
                    <div className="pt-2">
                      <h5 className="mt-4 font-weight-medium mb-0">Team Leader</h5>
                      <h6 className="subtitle mb-3">Su Mon Kyaw</h6>
                      <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 mb-4">
                  <div className="row">
                    <div className="col-md-12">
                      <img src="/team/team-3.jpg" alt="Kyaw Zin Thet" className="img-fluid rounded-circle" />
                    </div>
                    <div className="col-md-12 text-center">
                      <div className="pt-2">
                        <h5 className="mt-4 font-weight-medium mb-0">Senior Software Engineer</h5>
                        <h6 className="subtitle mb-3">Kyaw Zin Thet</h6>
                        <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-3 mb-4">
                    <div className="row">
                      <div className="col-md-12">
                        <img src="/team/team-4.jpg" alt="Yu Nathah" className="img-fluid rounded-circle" />
                      </div>
                      <div className="col-md-12 text-center">
                        <div className="pt-2">
                          <h5 className="mt-4 font-weight-medium mb-0">Senior Software Engineer</h5>
                          <h6 className="subtitle mb-3">Yu Nathah</h6>
                          <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-3 mb-4">
                    <div className="row">
                      <div className="col-md-12">
                        <img src="/team/team-5.jpg" alt="Shwe Yi Mon" className="img-fluid rounded-circle" />
                      </div>
                      <div className="col-md-12 text-center">
                        <div className="pt-2">
                          <h5 className="mt-4 font-weight-medium mb-0">Senior Software Engineer</h5>
                          <h6 className="subtitle mb-3">Shwe Yi Mon</h6>
                          <p>You can relay on our amazing features list and also our customer services will be great experience.</p>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
      </div>

    <section id="team" className="team section-bg">

            <div className="section-title" data-aos="fade-up">
                <h2>OUR Partners</h2>
                
            </div>
            <div className="row">
                <div className="col-lg-3 d-flex align-items-stretch">
                </div>
                <div className="col-lg-3 d-flex align-items-stretch">
                    <div className="member" data-aos="fade-up">
                        <div className="member-img">
                            <img src="/partner/Partner_1.png" className="img-fluid member-image" alt=""/>

                        </div>
                        <div className="member-info">
                            <h4>VMO Japan</h4>
                            <span>The Professional Software Development Service Provider Alternatives &amp; Competitors for Enterprise Businesses.</span>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 d-flex align-items-stretch">
                    <div className="member" data-aos="fade-up">
                        <div className="member-img">
                            <img src="/partner/Partner_2.png" className="img-fluid member-image" alt=""/>

                        </div>
                        <div className="member-info">
                            <h4>Calypso Technology</h4>
                            <span>global provider of capital markets software. Its integrated platform enables financial trading, processing, risk management and accounting across multiple asset classNamees and is used by over 35,000 market professionals in 60+ countries.</span>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 d-flex align-items-stretch">
                </div>
            </div>
    </section>
    <section id="clients" className="clients">
            <div className="section-title">
                <h2>Our Clients</h2>
            </div>
            <div className="row no-gutters clients-wrap clearfix justify-content-center" data-aos="fade-up">
                <div className="col-lg-2 col-md-2 col-4">
                    <div className="client-logo">
                        <img src="/clients/client-01.png" className="img-fluid" alt=""/>
                    </div>
                </div>
                <div className="col-lg-2 col-md-2 col-4">
                    <div className="client-logo">
                        <img src="/clients/client-02.png" className="img-fluid" alt=""/>
                    </div>
                </div>
                <div className="col-lg-2 col-md-2 col-4">
                    <div className="client-logo">
                        <img src="/clients/client-03.jpg" className="img-fluid" alt=""/>
                    </div>
                </div>
            </div>
    </section>

    </main>
    <Footer></Footer>
    </div>
  );

};

export default About
