import type { NextPage } from 'next'
import useTranslation from 'next-translate/useTranslation';
import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import { Card, Col, Container, Dropdown, DropdownButton, Form, Nav, Row } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

import { useEffect } from "react";
const Contact: NextPage = () => {
    const { t } = useTranslation();

    return (
        <div className='antialiased'>
            <Navbar></Navbar>
            <div className="mb-5">
                <div className="map-section">
                    <div className="gmap_canvas">
                        <iframe
                            style={{
                                border: 0,
                                width: '100%',
                                height: '350px'
                            }}
                            src="https://maps.google.com/maps?q=Nawaday%20Street&t=&z=13&ie=UTF8&iwloc=&output=embed"
                        ></iframe>
                    </div>
                </div>
                <br /><br />
                <div className="section-title">
                    <h2>CONTACT</h2>
                </div>
                <Container>
                    <Row className='justify-content-center'>
                        <Col md="8">
                            <Card className='p-5 contact-card'>
                                <Form>
                                    <input
                                        type="text"
                                        className="form-control mb-5"
                                        id="name"
                                        name="name"
                                        placeholder="Enter Full Name"
                                    />
                                    <input
                                        type="text"
                                        className="form-control mt-5 mb-5"
                                        id="email"
                                        name="email"
                                        placeholder="Enter Email"
                                    />
                                    <input
                                        type="text"
                                        className="form-control mt-5 mb-5"
                                        id="subject"
                                        name="subject"
                                        placeholder="Subject"
                                    />
                                    <textarea
                                        id="message"
                                        className="form-control mt-5 mb-4"
                                        name="message"
                                        rows={4}
                                        cols={50}
                                        placeholder="Message"
                                    ></textarea
                                    >
                                    <div className="text-center">
                                        <input className="btn btn-info" type="submit" value="Send Message" />
                                    </div>
                                </Form>

                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
            <Footer></Footer>
        </div>
    );

};

export default Contact
