import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import useTranslation from 'next-translate/useTranslation';
import Navbar from './components/layout/navbar';
import Footer from './components/layout/footer';
import { Dropdown, DropdownButton, Nav } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Carousel from 'react-bootstrap/Carousel'
import dynamic from "next/dynamic";
import jQuery from "jquery";
import 'aos/dist/aos.css';
import { useEffect, useState } from "react";
import Link from 'next/link';
const Careers: NextPage = () => {
    const { t } = useTranslation();
    const [role, setRole] = useState<'senior' | 'junior' | 'intern' | ''>('');

    return (
        <>
            <div className='antialiased'>
                <Navbar></Navbar>
                <div className="container mb-5 mt-3" id="career">
                    <h2 className="text-info text-center mt-5">CAREERS</h2>
                    <div className="row mt-5">
                        <div className="col-lg-6">
                            <img src="/career/careerpath.png" alt=""
                                className="img-fluid" style={{ marginLeft: '15px' }} />
                        </div>
                        <div className="col-lg-6">
                            <h3 className="text-info">
                                Benefits
                            </h3>
                            <div className="entry-content text-a">
                                <ul>
                                    <li>Provide online course and having teacher for new technology and Japanese Language.</li>
                                    <li>Can work in Japan Country by considering the experience and ability etc..</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 mt-5">
                        <h3 className="text-center text-info">
                            Career Opportunities
                        </h3>
                    </div>
                    <div className="row d-flex justify-content-center text-center mt-5">
                        <div className="col-lg-7">
                            <div className="row">
                                <div className="salaryCard col-md-3 al-self-center">
                                    Basic <br /> Salary
                                </div>
                                <div className="col-md-1 al-self-center ml-3">
                                    <i className="fa fa-plus" style={{ fontSize: '25px', color: '#00A4CC' }}></i>
                                </div>
                                <div className="col-md-8 text-left">
                                    <ul>
                                        <li>
                                            Technical skill allowance
                                        </li>
                                        <li>
                                            Management skill allowance
                                        </li>
                                        <li>
                                            Qualification allowance (JLPT, ITPEC, IELTS..)
                                        </li>
                                        <li>
                                            Transportation Fees
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5 careerCard text-left">
                            <div className="row">
                                <div className="col-md-3 text-info">Working<br />Hour</div>
                                <div className="col-md-9">8:30AM ～ 5:30PM<br />
                                    (Lunch break:1hour,<br /> Regular working hours : 8hours)
                                </div>
                            </div>
                            <div className="row pt-2">
                                <div className="col-md-3 text-info">Holiday</div>
                                <div className="col-md-9">Saturday, Sunday, Public holidays
                                </div>
                            </div>
                            <div className="row pt-2">
                                <div className="col-md-3 text-info">Welfare</div>
                                <div className="col-md-9">Employee trip<br />
                                    Regular medical check-up
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row d-flex justify-content-center text-center pt-5">
                        <div className="col-md-12">
                            <h3 className="text-center text-info">
                                We are looking for human resource that meets the following facts
                            </h3>
                        </div>
                        <div className="col-md-8 text-left">
                            <ul>
                                <li>
                                    person who wants to perticipate in the Future Myanmar ICT development
                                </li>
                                <li>
                                    person who can present his own idea and can communicate and interact at ease
                                </li>
                                <li>
                                    person who can work with in team
                                </li>
                                <li>
                                    person who is enthusiastic and tries hard to develop own skills
                                </li>
                                <li>
                                    person who has future goals and ables to cope with challenges to achieve goals
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="row pt-5">
                        <div className="col-md-12">
                            <h3 className="text-center text-info">
                                Type of Recruiting
                            </h3>
                        </div>
                        <div className="card col-lg-12 mt-3">
                            <div className="row">
                                <div className="col-lg-7">
                                    <h4 className="text-left text-info">
                                        <span>&#128142;</span> Senior Developer
                                    </h4>
                                </div>
                                <div className="col-lg-5">
                                    <div className="smallCard">
                                        <span>&#128182; &nbsp;</span>Negotiable
                                        <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time
                                        &nbsp; &nbsp; &nbsp;<i className='fa fa-map-marker' style={{ color: 'red' }}></i> &nbsp; Myanmar-Yangon
                                    </div>
                                </div>
                            </div>
                            <div className="mt-3">
                                <ul>
                                    <li>
                                        Project experience over 4 years.
                                    </li>
                                    <li>
                                        At least 3 years in web programming experience with any programming language and MVC framework.
                                    </li>
                                    <li>
                                        Japanese language proficiency level N3 or higher.
                                    </li>
                                    <li>
                                        Work in a hands-on fashion, building the team - provide motivation and  Inspiration.
                                    </li>
                                </ul>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Link href="/careers/senior">
                                        <button className="btn btn-info">View Details</button>
                                    </Link>
                                    <button className="btn btn-warning">Apply Job</button>

                                </div>

                            </div>
                        </div>
                        <div className="card col-lg-12 mt-3">
                            <div className="row">
                                <div className="col-lg-7">
                                    <h4 className="text-left text-info">
                                        <span>&#128142;</span> Junior Developer
                                    </h4>
                                </div>
                                <div className="col-lg-5">
                                    <div className="smallCard">
                                        <span>&#128182; &nbsp;</span>Negotiable
                                        <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time
                                        &nbsp; &nbsp; &nbsp;<i className='fa fa-map-marker' style={{ color: 'red' }}></i> &nbsp; Myanmar-Yangon
                                    </div>
                                </div>
                            </div>
                            <div className="mt-3">
                                <ul>
                                    <li>
                                        Any Graduated or Diploma in IT.
                                    </li>
                                    <li>
                                        Strong technical skills and knowledge with the latest web development technology of Git.
                                    </li>
                                    <li>
                                        Good communication and interpersonal skills.
                                    </li>
                                </ul>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Link href="/careers/junior">
                                        <button className="btn btn-info">View Details</button>
                                    </Link>
                                    <button className="btn btn-warning">Apply Job</button>

                                </div>
                            </div>
                        </div>
                        <div className="card col-lg-12 mt-3">
                            <div className="row">
                                <div className="col-lg-7">
                                    <h4 className="text-left text-info">
                                        <span>&#128142;</span> Internship
                                    </h4>
                                </div>
                                <div className="col-lg-5">
                                    <div className="smallCard">
                                        <span>&#128182; &nbsp;</span>Negotiable
                                        <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time
                                        &nbsp; &nbsp; &nbsp;<i className='fa fa-map-marker' style={{ color: 'red' }}></i> &nbsp; Myanmar-Yangon
                                    </div>
                                </div>
                            </div>
                            <div className="mt-3">
                                <ul>
                                    <li>
                                        Any Graduated or Diploma in IT.
                                    </li>
                                    <li>
                                        Strong technical skills and knowledge with the latest web development technology of Git.
                                    </li>
                                    <li>
                                        Good communication and interpersonal skills.
                                    </li>
                                </ul>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <Link href="/careers/intern">
                                        <button className="btn btn-info">View Details</button>
                                    </Link>
                                    <button className="btn btn-warning">Apply Job</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer></Footer>
            </div>
        </>
    );

};

export default Careers
